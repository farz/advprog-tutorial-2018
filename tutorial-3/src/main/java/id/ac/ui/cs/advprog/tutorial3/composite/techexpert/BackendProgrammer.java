package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {

    //TODO Implement
    public BackendProgrammer(String name, double salary) {
        if (salary < 20000.00) {
            throw new IllegalArgumentException();
        } else {
            this.name = name;
            this.salary = salary;
            this.role = "Back End Programmer";
        }
    }

    @Override
    public double getSalary() {
        // TODO Auto-generated method stub
        return this.salary;
    }

}
