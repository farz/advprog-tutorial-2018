import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    private Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);
    }

    @Test
    public void testScoreGroupingContainsRightKey() {
        assertTrue(ScoreGrouping.groupByScores(scores).containsKey(11));
    }

    @Test
    public void testScoreGroupingContainsRightValue() {
        assertFalse(ScoreGrouping.groupByScores(scores).containsValue("G"));
    }

    @Test
    public void testScoreGroupingSizeIsEqual() {
        assertEquals(3, ScoreGrouping.groupByScores(scores).size());
    }

    @Test
    public void testMain() {
        ScoreGrouping.main(new String[]{});
    }
}